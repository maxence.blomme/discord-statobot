import redis
from singleton_decorator import singleton


@singleton
class StatoRedis:
    host = 'redis'
    port = 6379

    def __init__(self):
        self.connection = redis.Redis(host=self.host, port=self.port)

    def set(self, key, value, expiration=None):
        self.connection.set(key, value, ex=expiration)

    def get(self, key):
        return self.connection.get(key)

    def delete(self, key):
        return self.connection.delete(key)

    @staticmethod
    def statopolice_key(user_id, channel):
        return str(user_id) + "," + str(channel.id)

    def register_statopolice_message(self, user_id, sent_msg):
        self.set(self.statopolice_key(user_id, sent_msg.channel), sent_msg.id, expiration=1800)

    def get_registered_statopolice_message(self, user_id, channel):
        return self.get(self.statopolice_key(user_id, channel))
