import datetime
from threading import RLock

lock = RLock()

memoized_history = {}
last_clean = datetime.datetime.now()

shift_utc = 2  # Décalage avec l'heure UTC. En France : 1 à l'heure d'hiver et 2 à l'heure d'été

async def get_history(chan, date):
    global memoized_history
    with lock:
        date_start = datetime.datetime(day=date.day, month=date.month, year=date.year, hour=00, minute=00) - \
                    shift_utc * datetime.timedelta(hours=1)
        if (chan, date) in memoized_history:
            previous_history = memoized_history[(chan, date)]
            if len(previous_history) > 0:
                aft = previous_history[-1].created_at
            else:
                aft = date_start
            bef = date_start + datetime.timedelta(days=1)
            bef = bef if bef < datetime.datetime.now() else datetime.datetime.now()
            history = await chan.history(limit=None, before=bef, after=aft, oldest_first=True).flatten()
            history = previous_history + history
        else:
            aft = date_start
            bef = date_start + datetime.timedelta(days=1)
            bef = bef if bef < datetime.datetime.now() else datetime.datetime.now()
            history = await chan.history(limit=None, before=bef, after=aft, oldest_first=True).flatten()
        memoized_history[(chan, date)] = history
    clean_memoized_history()
    return history



def clean_memoized_history():
    global last_clean
    global memoized_history
    with lock:
        if len(memoized_history) > 1000 or last_clean + datetime.timedelta(days=7) < datetime.datetime.now():
            last_clean = datetime.datetime.now()
            memoized_history = {}
