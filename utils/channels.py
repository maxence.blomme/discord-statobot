import discord


def text_channels(guild):
    for chan in guild.channels:
        if not isinstance(chan, discord.TextChannel):
            continue
        yield chan
