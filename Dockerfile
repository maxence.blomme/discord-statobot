FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN mkdir /statobot

WORKDIR /statobot

RUN apk add --update alpine-sdk zlib-dev jpeg-dev openblas-dev

ADD requirements.txt /statobot/requirements.txt

RUN pip install -r requirements.txt

ADD . /statobot/

ENTRYPOINT ["python3", "statobot.py"]
