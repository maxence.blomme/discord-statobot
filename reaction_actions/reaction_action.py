from abc import ABC, abstractmethod


class AbstractReactionAction(ABC):
    def __init__(self):
        super().__init__()

    @staticmethod
    async def get_message(channel_id, message_id, client):
        return await client.get_channel(channel_id).fetch_message(message_id)

    @staticmethod
    def get_reaction_user(payload, client):
        return client.get_user(payload.user_id)

    @staticmethod
    @abstractmethod
    async def on_add(payload, client):
        """
        Method called when a reaction is added
        :param payload: raw event payload data
        :param client: Discord client object
        :return: No return value is expected
        """
        pass

    @staticmethod
    @abstractmethod
    async def on_remove(payload, client):
        """
        Method called when a reaction is removed
        :param payload: raw event payload data
        :param client: Discord client object
        :return: No return value is expected
        """
        pass
