from reaction_actions.reaction_action import AbstractReactionAction
from utils.statoredis import StatoRedis


class Statopolice(AbstractReactionAction):
    @staticmethod
    async def on_add(payload, client):
        if not Statopolice.authorized_guild(payload.guild_id):
            return

        msg = await AbstractReactionAction.get_message(payload.channel_id, payload.message_id, client)
        msg_user = msg.author
        if payload.user_id == msg_user.id:
            previous_message = StatoRedis().get_registered_statopolice_message(payload.user_id, msg.channel)
            last_message = (await msg.channel.history(limit=1).flatten())[0]
            if previous_message is None or int(previous_message) != last_message.id:
                user_tag = "<@%i>" % payload.user_id
                message = ":rotating_light: :police_officer: :rotating_light: STATOPOLICE :rotating_light: :police_officer: " \
                          ":rotating_light:\n "
                message += "%s, vous êtes surpris en flagrant délit d'auto-réaction. Veuillez cesser immédiatement ce " \
                           "comportement de faussage de statistiques ! :angry: "
                sent_msg = await msg.channel.send(message % user_tag)
                StatoRedis().register_statopolice_message(payload.user_id, sent_msg)

                if previous_message is not None:
                    await (await AbstractReactionAction.get_message(payload.channel_id, int(previous_message), client)).delete()

    @staticmethod
    async def on_remove(payload, client):
        pass

    @staticmethod
    def authorized_guild(guild_id):
        return StatoRedis().get(guild_id) is None
