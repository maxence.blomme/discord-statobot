import datetime

from actions.action import AbstractAction
from utils.channels import text_channels
from utils.date import parse_date
from utils.history_memoization import get_history


class Leaderboard(AbstractAction):

    @staticmethod
    def command():
        return "!leaderboard"

    @staticmethod
    def command_short():
        return "!lb"

    @staticmethod
    def help_description():
        return "Afficher le leaderboard"

    @staticmethod
    def help_args():
        return ["", "jj/mm/aaaa"]

    @staticmethod
    async def on_call(message, client):
        splitted = message.content.split()
        if len(splitted) == 1:
            async with message.channel.typing():
                await Leaderboard.send_leaderboard(client, message.guild, message.channel, datetime.date.today())
        elif len(splitted) == 2:
            date = parse_date(splitted[1])
            if date is None:
                await message.channel.send("*Invalid date format*")
            else:
                async with message.channel.typing():
                    await Leaderboard.send_leaderboard(client, message.guild, message.channel, date)
        else:
            await message.channel.send("*Invalid argument amount*")

    @staticmethod
    async def send_leaderboard(client, guild, channel, date):
        day_info = {}

        for chan in text_channels(guild):
            try:
                for message in await get_history(chan, date):
                    if message.author == client.user:
                        continue
                    if message.author not in day_info:
                        day_info[message.author] = 0
                    day_info[message.author] += 1
            except:
                pass

        text = "**Voici les gagnants et perdants du " + datetime.datetime.strptime(str(date), '%Y-%m-%d').strftime('%d/%m/%Y') + "!**\n"
        keys = sorted(day_info.keys(), key=lambda key: day_info[key], reverse=True)
        for i, user in enumerate(keys):
            if i == 0:
                text += ":first_place: **{.name}** avec {} messages!\n".format(user, day_info[user])
            elif i == 1:
                text += ":second_place: **{.name}** avec {} messages!\n".format(user, day_info[user])
            elif i == 2:
                text += ":third_place: **{.name}** avec {} messages!\n".format(user, day_info[user])
                if len(keys) > 3:
                    text += "*Les perdants sont:*\n"
            else:
                text += '{}: **{.name}** avec {} messages\n'.format(i + 1, user, day_info[user])
        text += "\n*Felicitations tout le monde! Voici un cookie pour chacun d'entre vous:*\n"
        for _ in range(len(keys)):
            text += ":cookie:"
        await channel.send(text)
