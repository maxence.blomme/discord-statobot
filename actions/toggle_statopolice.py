from actions.action import AbstractAction
from utils.statoredis import StatoRedis


class ToggleStatopolice(AbstractAction):

    @staticmethod
    def command():
        return "!statopolice"

    @staticmethod
    def command_short():
        return "!sp"

    @staticmethod
    def help_description():
        return "Activer ou désactiver la Statopolice pour ce serveur"

    @staticmethod
    def help_args():
        return ["{on,off}"]

    @staticmethod
    async def on_call(message, client):
        guild_id = message.guild.id
        activated = StatoRedis().get(guild_id) is None

        splitted = message.content.split()
        if len(splitted) == 1:
            if activated:
                await ToggleStatopolice.disable_statopolice(guild_id, message.channel)
            else:
                await ToggleStatopolice.enable_statopolice(guild_id, message.channel)
        elif splitted[1] == "on":
            await ToggleStatopolice.enable_statopolice(guild_id, message.channel)
        elif splitted[1] == "off":
            await ToggleStatopolice.disable_statopolice(guild_id, message.channel)
        else:
            await message.channel.send("*Erreur : argument invalide*")


    @staticmethod
    async def enable_statopolice(guild_id, channel=None):
        StatoRedis().delete(guild_id)
        await channel.send("*Statopolice activée pour ce serveur*")

    @staticmethod
    async def disable_statopolice(guild_id, channel=None):
        StatoRedis().set(guild_id, 1)
        await channel.send("*Statopolice désactivée pour ce serveur*")
