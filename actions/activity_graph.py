import datetime
import os

import discord
from matplotlib import pyplot as plt

from actions.action import AbstractAction
from utils.channels import text_channels
from utils.date import parse_date
from utils.history_memoization import get_history, shift_utc


class ActivityGraph(AbstractAction):

    @staticmethod
    def command():
        return "!activity"

    @staticmethod
    def command_short():
        return "!ac"

    @staticmethod
    def help_description():
        return "Afficher le graphe d'activite"

    @staticmethod
    def help_args():
        return ["", "jj/mm/aaaa", "#@mentions/all", "jj/mm/aaaa #@mentions/all"]

    @staticmethod
    async def on_call(message, client):
        chans, users = None, None
        splitted = message.content.split()
        if len(splitted) == 1:
            async with message.channel.typing():
                await ActivityGraph.send_activity_graph(message.guild, message.channel, datetime.date.today(),
                                                        datetime.timedelta(minutes=30))
        elif len(splitted) >= 2:
            arg_len = len(splitted) - 3

            date = parse_date(splitted[1])
            date_specified = date is not None
            if not date_specified:
                date = datetime.date.today()
                arg_len += 1

            if "all" in splitted:
                show_all = True
            else:
                arg_len += 1
                show_all = date_specified and len(splitted) == 2

            if arg_len != len(message.mentions) + len(message.channel_mentions):
                await message.channel.send("*Arguments invalides*")
                return

            if len(message.channel_mentions) > 0:
                chans = message.channel_mentions
            if len(message.mentions) > 0:
                users = message.mentions

            async with message.channel.typing():
                await ActivityGraph.send_activity_graph(message.guild, message.channel, date,
                                                        datetime.timedelta(minutes=30), show_all, chans, users)
        else:
            await message.channel.send("*Nombre d'arguments invalide*")

    @staticmethod
    def get_intervals(times):
        for i in range(len(times)):
            if i < len(times) - 1:
                yield times[i], times[i + 1]

    @staticmethod
    async def send_activity_graph(guild, channel, date, delta, show_all=True, chans=None, users=None):
        absc = [" 00:00 "]

        aft = datetime.datetime(day=date.day, month=date.month, year=date.year, hour=00, minute=00) - \
              datetime.timedelta(hours=1)
        end = aft + datetime.timedelta(days=1)

        times = []
        while aft <= end:
            times.append(aft)
            aft += delta

        i = 0
        for aft, bef in ActivityGraph.get_intervals(times):
            if i % 8 == 7:
                absc.append("{:02d}".format((bef.hour + 1) % 24) + ":" + "{:02d}".format(bef.minute))
            else:
                absc.append(" " * i)
            i += 1

        i += 1
        ords = {}
        if show_all:
            ords["all"] = [0]*i
        if chans is not None:
            for chan in chans:
                ords[chan] = [0]*i
        if users is not None:
            for user in users:
                ords[user] = [0]*i

        start_date = datetime.datetime(day=date.day, month=date.month, year=date.year, hour=00, minute=00) - \
            shift_utc * datetime.timedelta(hours=1)

        for chan in text_channels(guild):
            try:
                for message in await get_history(chan, date):
                    index = 1
                    while message.created_at > start_date + index * delta:
                        index += 1
                    if show_all:
                        ords['all'][index] += 1
                    if chan in ords:
                        ords[chan][index] += 1
                    if message.author in ords:
                        ords[message.author][index] += 1
            except:
                pass

        plots = []
        if show_all:
            plots.append(plt.plot(absc, ords["all"], label="all")[0])
        if chans is not None:
            for chan in chans:
                plots.append(plt.plot(absc, ords[chan], label="#{.name}".format(chan))[0])
        if users is not None:
            for user in users:
                plots.append(plt.plot(absc, ords[user], label="@{.name}".format(user))[0])
        plt.legend(handles=plots)

        plt.savefig("activity.png")
        pic = open("activity.png", "rb")
        discord_pic = discord.File(pic)
        txt = "Voici le graphique d'activite du " + datetime.datetime.strptime(str(date), '%Y-%m-%d').strftime(
            '%d/%m/%Y')
        await channel.send(content=txt, file=discord_pic)
        discord_pic.close()
        pic.close()
        os.remove("activity.png")
        plt.clf()
